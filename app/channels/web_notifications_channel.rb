class WebNotificationsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "web_notifications_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
  
  def talk(data)
    # work, thu chuyen sang controller xem tnao

    post = Post.create(title: data['DuLieu'])
    post.image.attach(io: File.open("app/assets/images/cap.png"), filename: "face.png", content_type: "image/png")
    post.save
    ActionCable.server.broadcast "web_notifications_channel", noidung: data["DuLieu"], 
    hinhanh: "<img src= '#{post.image.service_url}'>" , post: render_post(post)   
  end

  def send_message(data)
    post = Post.create(title: 'aha')
    post.content = data['message']
    if data['file_uri']
      #post.image.attach = Base64.decode64(data['file_uri'])
      post.image.attach(io: StringIO.new(Base64.decode64(data['file_uri'])), filename: data['original_name'], content_type: "image/png")
      #post.image.attach(io: Base64.decode64(data['file_uri']), filename: "face.png", content_type: "image/png")
      #post.content = data['file_uri']
    end
    post.save
    ActionCable.server.broadcast "web_notifications_channel", noidung: data["message"], 
    post: render_post(post)   
  end
  
  def xoa(data)
    #Post.create(title: data['DuLieu'])
    Post.destroy_all
  end
  
  private
  def render_post(data)
    ApplicationController.renderer.render(partial: 'posts/image', locals: { post: data })
  end
end
