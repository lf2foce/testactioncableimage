App.web_notifications = App.cable.subscriptions.create "WebNotificationsChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    $('#messages').append data['noidung']
    #$('#messages').append data['hinhanh']
    $('#messages').append data['post']

  talk: (noidung) ->
    @perform 'talk', DuLieu: noidung

  xoa: (noidung) ->
    @perform 'xoa' , DuLieu: noidung
  
  send_message: (message, file_uri, original_name) ->
    @perform 'send_message', message: message, file_uri: file_uri, original_name: original_name  

#App.web_notifications.talk data['message']


  $(document).on "turbolinks:load", ->
    $('h1').click ->
      $('.container').fadeOut("slow")
      return
    
    $("#post_image").change () ->
      #alert('ahaha')
      message_body = $('#post_content').val()
      if $('#post_image').get(0).files.length > 0
        
        $('#post_content').val('')
        reader = new FileReader()
        file_name = $('#post_image').get(0).files[0].name
        reader.addEventListener "loadend", ->
          App.web_notifications.send_message message_body, reader.result.split(',')[1], file_name
        reader.readAsDataURL $('#post_image').get(0).files[0]
        $('#post_image').val('')
      else
        App.web_notifications.send_message message_body
