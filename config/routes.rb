Rails.application.routes.draw do
  get 'static_pages/home'
  get 'static_pages/about'
  resources :posts
  get 'posts/index'
  root 'posts#index'
  get  'static_pages/home'
  get '/blog', to: 'static_pages#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #mount ActionCable.server => '/cable'
end
